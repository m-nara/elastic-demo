# configure index mapping for sensor and httpd

```
curl -XPUT -H "Content-Type: application/json" -d @map.yml localhost:9200/_template/httpd
curl -XPUT -H "Content-Type: application/json" -d @sensormap.yml localhost:9200/_template/sensor
```
