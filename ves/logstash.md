## Origin Pool
Metadata
- Name: logstash
- Labels : ves.io/app_type = nos
Basic Configuration
- Select Type of Origin Server : k8s Service Name of Origin Server on given Sites
- Service Name : logstash.nos-demo
- Select Site or Virtual Site : Virtual Site
- Site : nos-demo/nos-datacenter
- Select Network on the site : Vk8s Networks on Site
- Port : 5045
- LoadBalancer Algorithm : Round Robin
- Endpoint Selection: Local Endpoints Preferred
---
## TCP Load Balancer
Metadata
- Name: logstash
- Labels : ves.io/app_type = nos
Basic Configuration
- Domains : logstash.nos-demo
- Listen Port : 5045
- Origin Pool : nos-demo/logstash
VIP Configuration
- Where to Advertise the VIP : Advertise Custom
  - Where to Advertise : Virtual Site
  - Site Network : Inside and Outside Netwrok
  - Virtual Site Reference : demo-branch
  - TCP Listen Port Choice : TCP Listen Port
  - TCP Listen Port : 5045
