## Origin Pool
Metadata
- Name: kibana
- Labels : ves.io/app_type = nos
Basic Configuration
- Select Type of Origin Server : IP address of Origin Server on given site
- IP : 10.44.59.5
- Select Site or Virtual Site : Virtual Site
- Virtual Site : nos-demo/nos-datacenter
- Select Network on the site : Outside Network
- Port : 5601
- LoadBalancer Algorithm : Round Robin
- Endpoint Selection: Local Endpoints Preferred
---
## HTTP Load Balancer
Metadata
- Name: elastcisearch
- Labels : ves.io/app_type = nos
Basic Configuration
- Domains : kibana.demo.netone.co.jp
- Type of Load Balancer : HTTP
Default Origin Servers
- Origin Pool : nos-demo/kibana
VIP Configuration
- Where to Advertise the VIP : Advertise On Internet
