## Origin Pool
Metadata
- Name: elasticsearch
- Labels : ves.io/app_type = nos
Basic Configuration
- Select Type of Origin Server : IP address of Origin Server on given site
- IP : 10.44.59.5
- Select Site or Virtual Site : Virtual Site
- Virtual Site : nos-demo/nos-datacenter
- Select Network on the site : Outside Network
- Port : 9200
- LoadBalancer Algorithm : Round Robin
- Endpoint Selection: Local Endpoints Preferred
---
## HTTP Load Balancer
Metadata
- Name: elastcisearch
- Labels : ves.io/app_type = nos
Basic Configuration
- Domains : elasticsearch.nos-demo
- Type of Load Balancer : HTTP
Default Origin Servers
- Origin Pool : nos-demo/elasticsearch
VIP Configuration
- Where to Advertise the VIP : Advertise Custom
  - Where to Advertise : Virtual Site
  - Site Network : Inside and Outside Netwrok
  - Virtual Site Reference : demo-datacenter
  - TCP Listen Port Choice : TCP Listen Port
  - TCP Listen Port : 9200
