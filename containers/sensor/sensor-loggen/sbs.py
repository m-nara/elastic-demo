# Copyright 2017 Amazon.com, Inc. or its affiliates. All Rights Reserved.
# SPDX-License-Identifier: MIT-0
    
# Script to generate simulated IoT device parameters data

import os
import json
import random
import datetime
import time
import socket

deviceNames = ['SBS01', 'SBS02', 'SBS03', 'SBS04', 'SBS05']

server = "filebeat"
#server = "192.168.0.57"
port = 9000

# generate Flow values
def getFlowValues():
    data = {}
    data['deviceValue'] = random.randint(60, 100)
    data['deviceParameter'] = 'Flow'
    data['deviceId'] = random.choice(deviceNames)
    data['timestamp'] = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    return data

# generate Temperature values
def getTemperatureValues():
    data = {}
    data['deviceValue'] = random.randint(15, 35)
    data['deviceParameter'] = 'Temperature'
    data['deviceId'] = random.choice(deviceNames)
    data['timestamp'] = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    return data

# generate Humidity values
def getHumidityValues():
    data = {}
    data['deviceValue'] = random.randint(50, 90)
    data['deviceParameter'] = 'Humidity'
    data['deviceId'] = random.choice(deviceNames)
    data['timestamp'] = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    return data

# generate Sound values
def getSoundValues():
    data = {}
    data['deviceValue'] = random.randint(100, 140)
    data['deviceParameter'] = 'Sound'
    data['deviceId'] = random.choice(deviceNames)
    data['timestamp'] = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    return data

def make_connection(host_, port_):
  while True:
    try:
      sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
      sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
      sock.connect((host_, port_))
      return sock
    except socket.error as e:
      print('failed to connect, try reconnect')
      time.sleep(1)

if __name__ == '__main__':
    client = make_connection(server,port)
    while True:
        try:
            time.sleep(0.5)
            rnd = random.random()
            if (0 <= rnd < 0.20):
                data = json.dumps(getFlowValues()).encode('utf-8')
                client.send(data)
            elif (0.20<= rnd < 0.55):
                data = json.dumps(getTemperatureValues()).encode('utf-8')
                client.send(data)
            elif (0.55<= rnd < 0.70):
                data = json.dumps(getHumidityValues()).encode('utf-8')
                client.send(data)
            else:
                data = json.dumps(getSoundValues()).encode('utf-8')
                client.send(data)
            client.send(b'\n')
        except socket.error as e:
            print('connection lost, try reconnect')
            client = make_connection(server, port)
